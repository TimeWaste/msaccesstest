package Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by chrono on 28/10/2015.
 */

@Entity
@Table(name = "dbo_TSDETMOV")
public class TesDetMovimientoEntity {

    @Id
    @Column(name = "Id")
    private Integer id;

    @Column(name = "CodMovimiento", length = 3, nullable = true)
    private String codMovimiento;

    @Column(name = "NumeroDocto", length = 20, nullable = true)
    private String nroDocumento;

    @Column(name = "NumeroReg", length = 3, nullable = true)
    private String nroRegistro;

    @Column(name = "CodModelo", length = 3, nullable = true)
    private String codModelo;

    @Column(name = "CodTitulo", length = 20, nullable = true)
    private String codTitulo;

    @Column(name = "Valor")
    private Double valor;

    @Column(name = "ValorBase")
    private Double valorBase;

    @Column(name = "DocumentoRef", length = 20, nullable = true)
    private String documentoReferencia;

    @Column(name = "IdUnoAcreedor", length = 20, nullable = true)
    private String idUnoAcreedor;

    @Column(name = "SucursalAcreedor", length = 3, nullable = true)
    private String sucursalAcreedor;

    @Column(name = "IdDosAcreedor", length = 20, nullable = true)
    private String idDosAcreedor;

    @Column(name = "CodCentroCostos", length = 3, nullable = true)
    private String codCentroCostos;

    @Column(name = "CodBanco", length = 3, nullable = true)
    private String codBanco;

    @Column(name = "Comentarios", length = 100, nullable = true)
    private String comentarios;

    @Column(name = "CodActivoFijo", length = 20, nullable = true)
    private String codActivoFijo;

    @Column(name = "EsIvaDescontable")
    private Boolean esIvaDescontable;

    @Column(name = "TipoAsiento", length = 1, nullable = true)
    private String tipoAsiento;


    public String getCodMovimiento() {
        return codMovimiento;
    }

    public void setCodMovimiento(String codMovimiento) {
        this.codMovimiento = codMovimiento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroRegistro() {
        return nroRegistro;
    }

    public void setNroRegistro(String nroRegistro) {
        this.nroRegistro = nroRegistro;
    }

    public String getCodModelo() {
        return codModelo;
    }

    public void setCodModelo(String codModelo) {
        this.codModelo = codModelo;
    }

    public String getCodTitulo() {
        return codTitulo;
    }

    public void setCodTitulo(String codTitulo) {
        this.codTitulo = codTitulo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValorBase() {
        return valorBase;
    }

    public void setValorBase(Double valorBase) {
        this.valorBase = valorBase;
    }

    public String getDocumentoReferencia() {
        return documentoReferencia;
    }

    public void setDocumentoReferencia(String documentoReferencia) {
        this.documentoReferencia = documentoReferencia;
    }

    public String getIdUnoAcreedor() {
        return idUnoAcreedor;
    }

    public void setIdUnoAcreedor(String idUnoAcreedor) {
        this.idUnoAcreedor = idUnoAcreedor;
    }

    public String getSucursalAcreedor() {
        return sucursalAcreedor;
    }

    public void setSucursalAcreedor(String sucursalAcreedor) {
        this.sucursalAcreedor = sucursalAcreedor;
    }

    public String getIdDosAcreedor() {
        return idDosAcreedor;
    }

    public void setIdDosAcreedor(String idDosAcreedor) {
        this.idDosAcreedor = idDosAcreedor;
    }

    public String getCodCentroCostos() {
        return codCentroCostos;
    }

    public void setCodCentroCostos(String codCentroCostos) {
        this.codCentroCostos = codCentroCostos;
    }

    public String getCodBanco() {
        return codBanco;
    }

    public void setCodBanco(String codBanco) {
        this.codBanco = codBanco;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getCodActivoFijo() {
        return codActivoFijo;
    }

    public void setCodActivoFijo(String codActivoFijo) {
        this.codActivoFijo = codActivoFijo;
    }

    public Boolean getEsIvaDescontable() {
        return esIvaDescontable;
    }

    public void setEsIvaDescontable(Boolean esIvaDescontable) {
        this.esIvaDescontable = esIvaDescontable;
    }

    public String getTipoAsiento() {
        return tipoAsiento;
    }

    public void setTipoAsiento(String tipoAsiento) {
        this.tipoAsiento = tipoAsiento;
    }

}
