import Config.NamingStrategy;
import Entities.TesDetMovimientoEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.model.naming.ImplicitNamingStrategy;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.cfg.Configuration;
import org.hibernate.jdbc.Work;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by chrono on 28/10/2015.
 */
class Main {

    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
                if ("Windows".equals(info.getName()) || "GTK+".equals(info.getName()))
                    UIManager.setLookAndFeel(info.getClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Main main = new Main();
        main.init();
    }

    private void init() {
        Configuration configuration = new Configuration();
        configuration.setPhysicalNamingStrategy(new NamingStrategy());
        configuration.setProperty("hibernate.connection.url", "jdbc:ucanaccess://D:/Test.accdb;memory=false");
        configuration.setPhysicalNamingStrategy((PhysicalNamingStrategy) new NamingStrategy());
        configuration.configure();

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                DatabaseMetaData metaData = connection.getMetaData();
                ResultSet resultSet = metaData.getTables(null, null, "%", new String[]{"Table"});
                while (resultSet.next())
                    System.out.println(resultSet.getString(3));
            }
        });

        Query fooQuery = session.createQuery("from TesDetMovimientoEntity where id = 5");
        List<TesDetMovimientoEntity> results = fooQuery.list();
        for (TesDetMovimientoEntity result : results)
            System.out.println(result.getComentarios());
    }
}
